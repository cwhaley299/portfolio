﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PWPage.Startup))]
namespace PWPage
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
