﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace codewars
{
    class Program
    {
        static void Main(string[] args)
        {

        }

        public static int GetLengthOfMissingArray(object[][] arrayOfArrays)
        {
            int result = 0;
            if (arrayOfArrays == null)
                return 0;

            int[] arrLengthHolder = new int[arrayOfArrays.Length];
            for (int i = 0; i < arrLengthHolder.Length; i++)
            {
                if (arrayOfArrays[i] == null || arrayOfArrays[i].Length == 0)
                    return 0;
                arrLengthHolder[i] = arrayOfArrays[i].Length;
            }
            arrLengthHolder = arrLengthHolder.OrderBy(i => i).ToArray();
            
            for (int c = 0; c < arrLengthHolder.Length - 1; c++)
            {
                if (arrLengthHolder[c] != (arrLengthHolder[c + 1] - 1))
                    result =  arrLengthHolder[c] + 1;
            }

            return result;
        }

        public static int ReturnEvenOrOddNumber(string numbers)
        {
            List<string> list = numbers.Split(' ').ToList();
            int result = 0;
            int even = 0;
            int odd = 0;

            foreach (var item in list)
            {
                if (int.Parse(item) % 2 == 0)
                {
                    even++;
                }
                else
                {
                    odd++;
                }
            }

            if (even == 1)
            {
                for (int i = 0; i < list.Count(); i++)
                {
                    if (int.Parse(list[i]) % 2 == 0)
                        result = i + 1;
                }
            }
            else
            {
                for (int i = 0; i < list.Count(); i++)
                {
                    if (int.Parse(list[i]) % 2 == 1)
                        result = i + 1;
                }
            }
            return result;
        }

        public static string[] dirReduc(string[] arr)
        {
            List<string> result = new List<string>();
            string north = "NORTH";
            int northCounter = 0;
            string south = "SOUTH";
            string west = "WEST";
            int westCounter = 0;
            string east = "EAST";

            if (arr[0] == north && arr[1] == west && arr[2] == south && arr[3] == east)
                return arr;

            foreach (var item in arr)
            {
                if (item == north)
                    northCounter++;
                if (item == south)
                    northCounter--;
                if (item == west)
                    westCounter++;
                if (item == east)
                    westCounter--;
            }

            if (northCounter != 0 && northCounter >= 1)
            {
                for (int i = 0; i < northCounter; i++)
                {
                    result.Add(north);
                }
            }
            else if (northCounter != 0 && northCounter <= -1)
            {
                for (int i = 0; i < -(northCounter); i++)
                {
                    result.Add(south);
                }
            }

            if (westCounter != 0 && westCounter >= 1)
            {
                for (int i = 0; i < westCounter; i++)
                {
                    result.Add(west);
                }
            }
            else if (westCounter != 0 && westCounter <= -1)
            {
                for (int i = 0; i < -(westCounter); i++)
                {
                    result.Add(east);
                }
            }

            return result.ToArray();


        }

        public static string[] FixTheMeerkat(string[] str)
        {
            return new string[] { str[2], str[1], str[0] };
        }
        public static int find_it(int[] seq)
        {
            //dictionary solution
            #region
            var items = new Dictionary<int, int>();

            foreach (var number in seq)
            {
                if (items.ContainsKey(number))
                {
                    items[number]++;
                }
                else
                    items.Add(number, 1);
            }
            var intNumbers = items.Keys.ToArray();
            var intOccurances = items.Values.ToArray();

            for (int c = 0; c < intOccurances.Count(); c++)
            {
                if (intOccurances[c] % 2 != 0)
                {
                    return intNumbers[c];
                }
            }
            #endregion
            // LINQ solution
            #region

            //return  seq.First(x => seq.Count(s => s == x) % 2 == 1); // not my answer, taken from the website
            //var linqGroup = seq.GroupBy(i => i)
            //    .Select(grp => new
            //    {
            //        number = grp.Key,
            //        total = grp.Count()
            //    })
            //    .ToArray();

            //for (int i = 0; i < linqGroup.Length; i++)
            //{
            //    if (linqGroup[i].total % 2 != 0)
            //    {
            //        return linqGroup[i].number;
            //    }
            //}
            #endregion

            return 0;
        }

        internal static string ToCamelCase(string str)
        {
            var holder = str.ToArray();
            List<int> spec = new List<int>();
            string result = string.Empty;

            for (int n = 0; n < holder.Length; n++)
            {
                if (holder[n] == '_' || holder[n] == '-')
                {
                    result += holder[n + 1].ToString().ToUpper();
                    n++;
                    continue;
                }
                result += holder[n].ToString();
            }
            return result;
        }

        internal static string GetMiddle(string s)
        {
            if (s.Length == 1)
                return s;
            else if (s.Length % 2 == 0)
            {
                int middleIndex = ((s.Length) + 1) / 2;
                char[] split = s.ToArray();
                return split[middleIndex - 1].ToString() + split[middleIndex].ToString();
            }
            else
            {
                return s.ToArray()[((s.Length + 1) / 2) - 1].ToString();
            }
        }

        internal static string SpinWords(string str)
        {
            StringBuilder result = new StringBuilder();
            string[] stringHolder = str.Split(' ');
            for (int c = 0; c < stringHolder.Length; c++)
            {
                if (stringHolder[c].Length > 4)
                {
                    var temp = stringHolder[c].ToArray().Reverse();
                    foreach (var item in temp)
                    {
                        result.Append(item);
                    }
                    if (c != stringHolder.Length - 1)
                    {
                        result.Append(" ");
                    }
                }
                else
                {
                    result.Append(stringHolder[c]);
                    if (c != stringHolder.Length - 1)
                    {
                        result.Append(" ");
                    }
                }
            }
            return result.ToString();
        }

        internal static string declareWinner(Fighter fighter1, Fighter fighter2, string firstAttacker)
        {
            int turn = 0;
            if (fighter1.Name == firstAttacker)
            {
                turn = 1;
            }
            for (;;)
            {
                if (turn % 2 == 1)
                {
                    fighter2.Health -= fighter1.DamagePerAttack;
                    if (fighter2.Health <= 0)
                    {
                        return fighter1.Name;
                    }
                }
                else
                {
                    fighter1.Health -= fighter2.DamagePerAttack;
                    if (fighter1.Health <= 0)
                    {
                        return fighter2.Name;
                    }
                }
                turn++;
            }
        }

        //This kata is not actually passing
        internal static char FindMissingLetter(char[] chr)
        {
            byte[] find = Encoding.ASCII.GetBytes(chr);

            for (int c = 0; c < find.Length - 1; c++)
            {
                if ((find[c] + 1) != find[c + 1])
                {
                    // return the next sequential char
                }
                else if (c == find.Length - 2)
                {
                    // end of comparison, return the next sequential from find[find.length - 1]
                }
            }
            return 'e';
        }

        internal static IEnumerable<string> FriendOrFoe(string[] names)
        {
            //List<string> linkResult = names.Where(n => n.Length == 4).ToList();
            return names.Where(n => n.Length == 4).ToList();
        }

        public static List<int> RemoveSmallest(List<int> numbers)
        {
            int lowVal = 0;
            if (numbers.Count() > 0)
            {
                for (int i = 0; i < numbers.Count() - 1; i++)
                {
                    if (numbers[i] < numbers[i + 1])
                    {
                        if (numbers[lowVal] > numbers[i])
                        {
                            lowVal = i;
                        }
                    }
                }
                numbers.RemoveAt(lowVal);
            }
            return numbers;
        }
    }
}
